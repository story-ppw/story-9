from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from .views import index, signup, signin, signout

app_name = 'authentication'

urlpatterns = [
    path('', index, name='index'),
    path('signup/', signup, name='signup'),
    path('signin/', signin, name='signin'),
    path('signout/', signout, name='signout'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
