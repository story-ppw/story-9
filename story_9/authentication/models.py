from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class UserAccount(AbstractUser):
   email = models.EmailField(unique=True)
   profile_image = models.ImageField(null=True, blank=True, upload_to="profile_image/")

   REQUIRED_FIELDS = ['email', 'password']

   class Meta:
      verbose_name = 'user'
      verbose_name_plural = 'users'
