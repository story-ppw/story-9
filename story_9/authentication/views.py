from django.shortcuts import redirect, render
from .forms import RegisterForm
from django.contrib.auth.forms import  AuthenticationForm
from django.contrib import messages
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login


def index(request):
    response = {}
    if request.user.is_authenticated: 
        response['username'] = request.user.get_username()
        response['email'] = request.user.email
        response['fullname'] = request.user.get_full_name()
        response['profile_image'] = request.user.profile_image
    return render(request, 'authentication/index.html', response)

def signup(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Account created successfully!")
            return redirect('authentication:signin')
    form = RegisterForm(request.POST or None)
    response = {'register_form': form}
    return render(request, 'authentication/signup.html', response)

def signin(request):
    form = AuthenticationForm(request.POST or None)
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        
        if user is not None:
            auth_login(request, user)
            # messages.success(request, "You have logged in successfully!")
            return redirect('authentication:index')
        else:
            messages.error(
                request, "No account can be found, please check your credentials or register first if you have not done so")
            return redirect('authentication:signin')
    response = {'login_form': form}
    return render(request, 'authentication/signin.html', response)

def signout(request):
    logout(request)
    return redirect('authentication:index')
