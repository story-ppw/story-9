from django.test import TestCase
from django.urls import reverse
from django.test import Client
from .views import signin, signup
from django.contrib.auth import get_user_model
from .forms import RegisterForm



class UnitTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.index_response = Client().get(reverse("authentication:index"))
        cls.signin_response = Client().get(reverse('authentication:signin'))
        cls.signup_response = Client().get(reverse('authentication:signup'))

    # Test status code

    def test_index_path_page_exist(self):
        self.assertEqual(self.index_response.status_code, 200)

    def test_login_path_page_exist(self):
        self.assertEqual(self.signin_response.status_code, 200)

    def test_register_path_page_exist(self):
        self.assertEqual(self.signup_response.status_code, 200)


    # Test template used

    def test_index_template_is_used(self):
        self.assertTemplateUsed(self.index_response, "authentication/index.html")
    
    def test_signin_template_is_used(self):
        self.assertTemplateUsed(self.signin_response, "authentication/signin.html")

    def test_signup_template_is_used(self):
        self.assertTemplateUsed(self.signup_response, "authentication/signup.html")

    
    # Test register account

    def test_register_account(self):
        # Test successful account creation
        response = self.client.post(reverse('authentication:signup'), {
                                    'username': "TestUsername2", 'email': "test2@email.com", 'password1': "testpassword2", 'password2': "testpassword2", 'first_name': 'Test', 'last_name': 'Test'}, follow=True)
        messages = list(response.context['messages'])
        self.assertEqual(str(messages[0]), 'Account created successfully!')
        self.assertEqual(get_user_model().objects.all().count(), 1)

        # Test failed account creation because of already existing username
        form_data = {'username': "TestUsername2", 'email': "test3@email.com",
                        'password1': "testpassword2", 'password2': "testpassword2", 'first_name': 'Test', 'last_name': 'Test'}
        form = RegisterForm(data = form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('A user with that username already exists.', form.errors['username'])

        # Test failed account creation because of already existing email
        form_data = {'username': "TestUsername3", 'email': "test2@email.com",
                        'password1': "testpassword2", 'password2': "testpassword2", 'first_name': 'Test', 'last_name': 'Test'}
        form = RegisterForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('User with this Email already exists.', form.errors['email'])

        # Test failed account creation because of already existing email
        form_data = {'username': "TestUserame3", 'email': "test3@email.com",
                        'password1': "testpassword2", 'password2': "testpassword3", 'first_name': 'Test', 'last_name': 'Test'}
        form = RegisterForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('The two password fields didn’t match.', form.errors['password2'])

        # Test failed account creation because of username containing illegal character
        form_data = {'username': "[][]]", 'email': "test3@email.com",
                        'password1': "testpassword2", 'password2': "testpassword2", 'first_name': 'Test', 'last_name': 'Test'}
        form = RegisterForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters.',
                        form.errors['username'])

        # Test if redirections works
        self.assertRedirects(response, reverse('authentication:signin'))

        # Test if the request is not in POST (in GET)
        response = self.client.get(reverse('authentication:signup'))
        self.assertEqual(response.status_code, 200)


    # Test login acount
    def test_login_account(self):
        # Prepare user account
        get_user_model().objects.create_user(username="TestName", password="testpassword")

        # Test successful login
        response = self.client.post(reverse('authentication:signin'), {'username': "TestName", 'password': "testpassword"}, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertRedirects(response, reverse('authentication:index'))

        # Test failed login because of wrong credentials
        response = self.client.post(reverse('authentication:signin'), {
                                    'username': "TestName2", 'password': "testpassword2"}, follow=True)
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'No account can be found, please check your credentials or register first if you have not done so')
        self.assertRedirects(response, reverse('authentication:signin'))

        # Test if the request is not in POST (in GET)
        response = self.client.get(reverse('authentication:signin'))
        self.assertEqual(response.status_code, 200)


    # Test logout

    def test_logout(self):
        # Prepare user account
        get_user_model().objects.create_user(username="TestName", password="testpassword")

        # Login
        self.client.post(reverse('authentication:signin'), {
                         'username': "TestName", 'password': "testpassword"}, follow=True)

        response = self.client.get(reverse('authentication:signout'))

        # Test if redirections works
        self.assertRedirects(response, reverse('authentication:index'))


    # Test if essential text and button exist in login and register page

    def test_important_text_in_login_page(self):
        self.assertContains(self.signin_response, 'Sign In')
        self.assertContains(self.signin_response, 'Username')
        self.assertContains(self.signin_response, 'Password')

    def test_important_text_in_register_page(self):
        self.assertContains(self.signup_response, 'Sign Up')
        self.assertContains(self.signup_response, 'Username')
        self.assertContains(self.signup_response, 'Email')
        self.assertContains(self.signup_response, 'First name')
        self.assertContains(self.signup_response, 'Last name')
