# Story 9

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

A simple authentication using Django

[pipeline-badge]: https://gitlab.com/story-ppw/story-9/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/story-ppw/story-9/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/story-ppw/story-9/-/commits/master
